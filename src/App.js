import React ,{Fragment, useState, useEffect} from 'react';
import BarraNavegadora from './componets/BarraNavegadora';
import TableNoticia from './componets/TableNoticia';


function App() {

const [listaNoticias, setListNotice] = useState([]);

const obtenerDatos = async ()=>{
  //este llamado es al servidor express.
  const apicall = await fetch('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
  const data = await apicall.json(); 
  setListNotice(data.hits); 
}
useEffect(() => {
  obtenerDatos();
}, [])

  return (
    <Fragment>
      <BarraNavegadora>
      </BarraNavegadora>
      <TableNoticia
        listaNoticias= {listaNoticias}
        setListNotice ={setListNotice}
      />
    </Fragment>
  );
}

export default App;
