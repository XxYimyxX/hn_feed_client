import React from 'react';
import FilaTable from './Row';
import {TableBody, Grid, Table, TableContainer} from '@material-ui/core';

const TableNoticia = ({listaNoticias, setListNotice}) => {
    //revisar la listaNoticia
    listaNoticias = listaNoticias.filter((noticia) => {
        if (noticia.title !== null && noticia.story_title !== null){
            return false;
        }else{
            return true;
        }
    });

    //Funcion que elimina la noticia.
    const eliminarNotica = id =>{
        const nuevaLista = listaNoticias.filter(item => item.objectID !== id)
        setListNotice(nuevaLista);
    }

    return ( 
        <Grid item xl={12}>
                <TableContainer >
                    <Table aria-label="simple table">
                        <TableBody>
                            {listaNoticias.map((noticia) => (
                            <FilaTable
                                key = {noticia.objectID}
                                noticia = {noticia}
                                eliminarNotica={eliminarNotica}
                            >  
                            </FilaTable>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid> 
     );
}
 
export default TableNoticia;
