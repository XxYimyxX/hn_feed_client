import React from 'react';
import {AppBar, Toolbar, Typography} from '@material-ui/core';
        
const BarraNavegadora = () => {
    return ( 
        <div>
            <AppBar position="static" color="primary">
              <Toolbar>
                <Typography variant="h3">
                  HN Feed 
                </Typography>
              </Toolbar>
            </AppBar>
        </div>
     );
}
 
export default BarraNavegadora