import React from 'react';
import {Icon, TableCell, TableRow, Button} from '@material-ui/core';
import moment from 'moment';


const Row = ({noticia, eliminarNotica}) => {
    return (   
        <TableRow>
            {noticia.title === null ? 
                <TableCell onClick={()=>{window.open(noticia.story_url)}}>{noticia.story_title} - <strong>{noticia.author}</strong></TableCell>
                :
                <TableCell onClick={()=>{window.open(noticia.story_url)}}>{noticia.title} - <strong>{noticia.author}</strong></TableCell>
            }
            <TableCell align="center">{moment(noticia.created_at).fromNow()}</TableCell>
            {noticia.id === 0 ? 
            <TableCell align="right">Eliminar de sus Favoritos</TableCell> 
            :
            <TableCell align="right">
                <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<Icon>delete</Icon>}
                    onClick={()=> eliminarNotica(noticia.objectID) }
                    >
                    Eliminar
                    </Button>
            </TableCell>
            }
        </TableRow>
     );
}
 
export default Row;